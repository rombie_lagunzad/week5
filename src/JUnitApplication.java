/*

Rombie Lagunzad
CIT 360
Brother Lawrence

This simple program is using JUnit in testing program.
Using assert methods in Junit, we can test the objects and logic returned from other methods

 */

import org.junit.Assert;
import org.junit.Test;

public class JUnitApplication {

    // Asserts that the two objects refer to the same object.
    @Test
    public void assertSameMethod() {
        System.out.println("\n***** assertSame Method *****");
        ExampleTest players = new ExampleTest();
        Assert.assertSame(players.onlineGame("Warrior"),players.onlineGame("Warrior"));
    }

    // Asserts that the two objects do not refer to the same object.
    @Test
    public void assertNotSameMethod() {
        System.out.println("\n***** assertNotSame Method *****");
        ExampleTest players = new ExampleTest();
        Assert.assertNotSame(players.onlineGame("Archer"),players.onlineGame("Sorcerer"));
    }

    // Asserts that a condition is false.
    @Test
    public void assertFalseMethod() {
        System.out.println("\n***** assertFalse Method *****");
        ExampleTest freezeResistance = new ExampleTest();
        Assert.assertFalse(freezeResistance.resist(5000));
    }

    // Asserts that a condition is true.
    @Test
    public void assertTrueMethod() {
        System.out.println("\n***** assertTrue Method *****");
        ExampleTest freezeResistance = new ExampleTest();
        Assert.assertTrue(freezeResistance.resist(12000));
    }

    // Asserts that an object is not null.
    @Test
    public void assertNotNullMethod() {
        System.out.println("\n***** assertNotNull Method *****");
        ExampleTest player = new ExampleTest();
        Assert.assertNotNull(player.onlineGame("Warrior"));
    }

    // Asserts that an object is null.
    @Test
    public void assertNullMethod() {
        System.out.println("\n***** assertNull Method *****");
        ExampleTest player = new ExampleTest();
        Assert.assertNull(player.onlineGame("Slayer"));
    }

    // Asserts that two values are equal.
    @Test
    public void assertEqualsMethod() {
        System.out.println("\n***** assertEquals Method *****");
        ExampleTest damage = new ExampleTest();
        int totalDamage = damage.reflectDamage(500,100);
        Assert.assertEquals(5, totalDamage);
    }

    // Asserts that the array expected and the resulted array are equal
    @Test
    public void assertArrayEqualsMethod () {
        System.out.println("\n***** assertArrayEquals Method *****");
        ExampleTest players = new ExampleTest();
        String[] playerClass = {"Academic", "Cleric", "Slayer"};
        String[] expectedClass = players.gameCharacters("Academic", "Cleric", "Slayer");
        Assert.assertArrayEquals(playerClass, expectedClass);
    }
}

