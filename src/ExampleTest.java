import java.util.HashMap;
import java.util.Map;

public class ExampleTest {

    public int reflectDamage (int attack, int hp) {

        int damage = 0;

        try {
            damage = (attack / hp);
        }

        catch (ArithmeticException ae) {
            System.out.println("HP cannot be zero. Try again!");
        }

        return damage;
    }

    public String[] gameCharacters (String character1, String character2, String character3) {
        String[] playerClass = {character1, character2, character3};
        return playerClass;
    }

    public String onlineGame (String key) {
        Map<String, String> player = new HashMap();
        player.put("Warrior","Attack");
        player.put("Sorcerer","Magic");
        player.put("Archer","Heal");
        return player.get(key);
    }

    public boolean resist(int freezeResist) {

        boolean outcome = true;
        if (freezeResist < 10000) {
            outcome = false;
        }
        return outcome;
    }
}
